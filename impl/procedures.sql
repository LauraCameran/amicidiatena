/* Procedure supplementari */
DELIMITER |
CREATE PROCEDURE dropTab (NTab VARCHAR(30))
-- Crea un istruzione di DROP TABLE in base a un nome NTab
BEGIN
	set @A = concat('DROP TABLE IF EXISTS ', NTab); 
	PREPARE dropT from @A;
	EXECUTE dropT;
	DEALLOCATE PREPARE dropT;
END|
DELIMITER ;

DELIMITER |
CREATE PROCEDURE TrovaAntenati(DISC_ID CHAR(4))
/* La procedura crea una tabella temporanea in cui vengono inseriti 
ricorsivamente tutti gli antenati di un personaggio il cui Codice è DISC_ID */
BEGIN
	-- Dichiarazioni e inizializzazioni
	DECLARE G1 CHAR(4);
	DECLARE G2 CHAR(4);
	SET G1 = NULL;
	SET G2 = NULL;

	-- Crea la tabella temporanea se non e' stata gia' creata
	CREATE TEMPORARY TABLE IF NOT EXISTS Antenati (Antenato CHAR(4));

	-- Trovo il genitore 1 e ne salvo il codice in G1
	SELECT Genitore INTO G1
	FROM Genitorialita
	WHERE Figlio = DISC_ID
	LIMIT 1;

	-- Trovo il genitore 2 e ne salvo il codice in G2
	SELECT Genitore INTO G2
	FROM Genitorialita
	WHERE Figlio = DISC_ID AND Genitore <> G1
	LIMIT 1;

	-- Ho trovato il genitore 1 se e solo se G1 e' diverso da NULL  
	-- Analogamente, ho trovato il genitore 2 se e solo se G2 diverso da NULL

	-- Se G1 è diverso da NULL e G1 non è già presente in Antenati
	IF G1 IS NOT NULL AND G1 NOT IN (SELECT * FROM Antenati)
	THEN
		INSERT INTO Antenati values (G1);	-- Inserisco G1 in Antenati
		call TrovaAntenati(G1);			-- Cerco gli antenati di G1
	END IF;

	-- Analogo al caso precedente
	IF G2 IS NOT NULL AND G2 NOT IN (SELECT * FROM Antenati)
	THEN
		INSERT INTO Antenati values (G2);
		call TrovaAntenati(G2);
	END IF;

	-- La ricorsione termina quando G1 e G2 sono entrambi NULL
END|
DELIMITER ;

DELIMITER |
CREATE PROCEDURE TrovaDiscendenti (ANT_ID CHAR(4))
/* La procedura crea una tabella temporanea il cui nome e' Discendenti 
in cui vengono inseriti iterativamente tutti i discendenti 
del personaggio il cui codice e' pari a ANT_ID */
BEGIN
	-- Dichiarazioni e inizializzazione di F
	DECLARE cont INTEGER; -- contatore
	DECLARE F CHAR(4); -- variabile Figlio

	-- Crea due tabelle temporanee, una che conterra' gli ID ancora da 
	-- controllare e l'altra che conterra' all'inizio tutti gli ID dei figli, 
	-- che verrano eliminati se non saranno discendenti
	CREATE TEMPORARY TABLE IF NOT EXISTS DaControllare AS (SELECT DISTINCT Figlio AS X FROM Genitorialita);
	CREATE TEMPORARY TABLE IF NOT EXISTS Discendenti AS (SELECT DISTINCT Figlio AS Discendente FROM Genitorialita);

	-- Inizializzazione di cont e F
	SELECT count(*) INTO cont FROM DaControllare;
	SELECT DISTINCT Figlio INTO F FROM Genitorialita LIMIT 1;
	WHILE (cont > 0)
	DO
		call TrovaAntenati(F);  -- Trovo tutti gli antenati di F

		-- Se gli antenati di F non contengono ANT_ID, 
		-- allora F non è un discendente
		IF ANT_ID NOT IN (SELECT * FROM Antenati)
		THEN
			-- Elimina F e tutti i suoi antenati dai Discendenti
			-- Infatti, se F non è discendente di ANT_ID, neanche i suoi 
			-- antenati lo saranno
			DELETE FROM Discendenti
			WHERE Discendente = F OR Discendente IN (SELECT * FROM Antenati);
		END IF;
		-- Esegue il DROP della tabella creata da TrovaAntenati(F)
		DROP TABLE Antenati;

		-- Elimina dagli elementi da controllare il codice F corrente e 
		-- tutti gli elementi che non appartengono piu' all'insieme dei Discendenti
		DELETE FROM DaControllare WHERE X = F OR X NOT IN (SELECT * FROM Discendenti);

		-- Assegna a F un nuovo ID
		SELECT X INTO F
		FROM DaControllare
		LIMIT 1;

		-- Riassegna a cont un nuovo valore che sara' minore o uguale a cont-1 
		-- dipendentemente da quanti elementi sono stati eliminati da DaControllare
		SELECT count(*) INTO cont FROM DaControllare;
	END WHILE;

	-- Esegue il DROP della tabella creata all'inizio;
	DROP TABLE DaControllare;
END|
DELIMITER ;

/* Operazione 3 */
DELIMITER |
CREATE PROCEDURE Aggiorna476(E VARCHAR(40))
/* aggiorna l’ente di riferimento di ogni sito archeologico le cui fonti hanno 
una data di creazione successiva al 476 d.C. */
BEGIN
	UPDATE SitoArcheologico SET Ente = E 
	WHERE Codice IN ( 	SELECT FA.CodiceSito
						FROM FonteArcheologica AS FA
									JOIN
							Fonte AS F ON FA.Codice = F.Codice
						WHERE F.Data > 476	);
END|
DELIMITER ;

/* Operazione 4 */
DELIMITER |
CREATE PROCEDURE inserisciAutoreFonte(ACodice CHAR(4), ANome VARCHAR(15), 
										FCodice CHAR (5), FTitolo VARCHAR(30),	
										FSito CHAR(3), Proven VARCHAR(10), 
										Scritta BOOLEAN)
/* inserisce un autore e la relativa fonte, se, nel caso in cui sia scritta, la 
sua provenienza è bizantina o, nel caso in cui sia archeologica, l’ente di 
riferimento del sito fornito ne gestisce solo uno */
BEGIN
	DECLARE Nsiti INTEGER;
	DECLARE E VARCHAR(40);
	
	-- Se la fonte è scritta e di provenienza 'Bizantina', è possibile procedere
	-- con l'inserimento
	IF Scritta AND Proven = 'Bizantina'
	THEN
		INSERT INTO Autore(Codice, Nome) VALUES (ACodice, ANome);
		INSERT INTO Fonte(Codice, Titolo, CodiceAutore) 
											VALUES (FCodice, FTitolo, ACodice);
		INSERT INTO FonteScritta VALUES (FCodice, Proven);
	END IF;
	
	-- Se la fonte non è scritta, conta il numero di siti gestiti dall'ente
	-- del sito fornito
	IF NOT Scritta
	THEN 
		-- Trova l'ente del sito fornito
		SELECT Ente INTO E
		FROM SitoArcheologico
		WHERE Codice = FSito;
	
		-- Conta i siti gestiti dall'ente E
		SELECT count(*) INTO Nsiti 
		FROM SitoArcheologico AS S
		WHERE Ente = E;
		
		-- Se il numero dei siti è 1, è possibile procedere con l'inserimento
		IF Nsiti = 1
		THEN
			INSERT INTO Autore(Codice, Nome) VALUES (ACodice, ANome);
			INSERT INTO Fonte(Codice, Titolo, CodiceAutore)
											VALUES (FCodice, FTitolo, ACodice);
			INSERT INTO FonteArcheologica(Codice, CodiceSito) 
											VALUES (FCodice, FSito);
		END IF;
	END IF;	
END|
DELIMITER ;

/* Operazione 5 */
DELIMITER |
CREATE PROCEDURE CancOpereFrancesi ()
/* cancella tutti i personaggi rappresentati in opere letterarie francesi */
BEGIN
	DELETE FROM Personaggio WHERE Codice IN 
				(	SELECT I.CodicePersonaggio
					FROM RapprModerna as RM, Ispirazione as I
					WHERE (RM.Tipo = 'Letteraria' AND RM.Nazionalita = 'FR') 
							AND I.CodiceRapp = RM.Codice	);
END|
DELIMITER ;

/* Operazione 10 */
DELIMITER |
CREATE PROCEDURE GenitorePersonaggio (x integer, y integer) 
/* dati x e y, stampa i nomi dei genitori di tutti i personaggi che hanno minimo 
x appellativi e massimo y appellativi */
BEGIN
	SELECT DISTINCT Nome_Genitore
	FROM(	SELECT  PE.Codice AS CodFig, PE.Nome AS Nome_Figlio,
					Gen.Nome AS Nome_Genitore
			FROM ((Personaggio as PE 
						LEFT JOIN 
					Epiteto as EP ON EP.CodicePersonaggio = PE.Codice)
						JOIN 
					Genitorialita as GE ON GE.Figlio = PE.Codice)
						JOIN 
					Personaggio as Gen ON GE.Genitore = Gen.Codice
			GROUP BY PE.Codice, PE.Nome, Gen.Nome  -- raggruppiamo per il codice del figlio
			HAVING count(EP.Appellativo) >= x AND count(EP.Appellativo) <= y 
			-- il figlio deve avere almeno x appellativi e al massimo y appellativi
		) AS Figli;
END|
DELIMITER ;

/* Operazione 11 */
DELIMITER |
CREATE PROCEDURE OperePersonaggioPrimaDiZ (Z SMALLINT)
/* dato z, stampa titolo e autore di tutte le rappresentazioni moderne 
letterarie che hanno come protagonista un personaggio delineato o narrato in 
almeno una fonte scritta prima dell'anno z */
BEGIN
	SELECT DISTINCT Titolo, AutoreModerno AS Autore
	FROM ((	-- Questa subquery trova titolo, autore della rappresentazione e 
			-- data della fonti presenti in Narrazione
			SELECT RM.Titolo, RM.AutoreModerno, F.Data AS DataFonte
			FROM ((((RapprModerna AS RM 
						JOIN 
					Ispirazione AS I ON RM.Codice = I.CodiceRapp)
						JOIN
					Coinvolgimento AS C ON I.CodicePersonaggio = C.CodicePersonaggio)
						JOIN
					Narrazione AS N ON C.CodiceEvento = N.CodiceEvento)
						JOIN
					FonteScritta AS FS ON N.CodiceFonte = FS.Codice)
						JOIN
					Fonte AS F ON FS.Codice = F.Codice)
			UNION
		(	-- Questa subquery trova titolo, autore della rappresentazione e 
			-- data delle fonti presenti in Delineazione
			SELECT RM.Titolo, RM.AutoreModerno, F.Data
			FROM (((RapprModerna AS RM 
					JOIN 
				Ispirazione AS I ON RM.Codice = I.CodiceRapp)
					JOIN
				Delineazione AS D ON I.CodicePersonaggio = D.CodicePersonaggio)
					JOIN
				FonteScritta AS FS ON D.CodiceFonte = FS.Codice)
					JOIN
				Fonte AS F ON FS.Codice = F.Codice )) AS U
	WHERE U.DataFonte < Z; -- Seleziona solo le fonti scritte prima dell'anno Z
END|
DELIMITER ;

/* Operazione 15 */
DELIMITER |
CREATE PROCEDURE creaTabellaAntenati(DISC_ID CHAR(4))
/* dato un personaggio DISC_ID, crea una tabella, il cui nome è, 
Antenati_di_DISC_ID, che contiene tutti i suoi antenati */
BEGIN
	-- Effettua il DROP della tabella
	call dropTab(concat('Antenati_di_',DISC_ID));
	
	-- Aumenta il limite di ricorsione
	set max_sp_recursion_depth = 50;
	
	-- Chiama TrovaAntenati con DISC_ID come parametro
	call TrovaAntenati(DISC_ID);
	
	-- Crea una stringa che contiene un'istruzione per rinominare la tabella 
	-- Antenati creata da TrovaAntenati
	set @A = concat('ALTER TABLE Antenati RENAME TO Antenati_di_',DISC_ID);
	PREPARE tabToTempTab from @A; -- Prepara l'istruzione
	EXECUTE tabToTempTab; -- Esegue
	DEALLOCATE PREPARE tabToTempTab; -- Dealloca istruzione preparata
	
	-- Reimposta il limite di ricorsione al valore di default
	set max_sp_recursion_depth = 0;
END|
DELIMITER ;

/* Operazione 16 */
DELIMITER |
CREATE PROCEDURE creaTabellaDiscendenti(ANT_ID CHAR(4))
/* La procedura crea una tabella chiamata Discendenti_di_ANT_ID con tutti i 
discendenti di ANT_ID */
BEGIN
	-- Effettua il DROP della tabella
	call dropTab(concat('Discendenti_di_', ANT_ID));
	
	-- Aumenta il limite di ricorsione
	set max_sp_recursion_depth = 50;
	
	-- Chiama TrovaDiscendenti con ANT_ID come parametro
	call TrovaDiscendenti(ANT_ID);
	
	-- Crea una stringa che contiene un'istruzione per rinominare la tabella 
	-- Antenati creata da TrovaAntenati
	set @A = concat('ALTER TABLE Discendenti RENAME TO Discendenti_di_',ANT_ID);
	PREPARE rinomina from @A; -- Prepara l'istruzione
	EXECUTE rinomina; -- Esegue
	DEALLOCATE PREPARE rinomina; -- Dealloca l'istruzione preparata
	
	-- Reimposta il limite di ricorsione al valore di default
	set max_sp_recursion_depth = 0;
END|
DELIMITER ;

/* Operazione 17 */
DELIMITER |
CREATE PROCEDURE GenealogiaComune(Pers1 CHAR(4), Pers2 CHAR(4))
/* La procedura trova tutti i codici dei personaggi che si trovano sia tra gli 
antenati o discendenti di Pers1 che tra gli antenati o discendenti di Pers2 */
BEGIN
	
	-- Crea le tabelle degli antenati e dei discendenti dei due personaggi
	call creaTabellaAntenati(Pers1);
	call creaTabellaDiscendenti(Pers1);

	call creaTabellaAntenati(Pers2);
	call creaTabellaDiscendenti(Pers2);
/*	
	La SELECT da eseguire (inserita nel concat seguente) è:
	SELECT A.Antenato AS InComune
	FROM (	SELECT * FROM Antenati_di_Pers1 
					UNION 
			SELECT * FROM Discendenti_di_Pers1) AS A,
		( 	SELECT * FROM Antenati_di_Pers2
					UNION
			SELECT * FROM Discendenti_di_Pers2) AS B
	WHERE A.Antenato = B.Antenato;
*/
	-- Effettua la select voluta
	set @A = concat('SELECT A.Antenato AS InComune FROM (SELECT * FROM Antenati_di_', Pers1, ' UNION SELECT * FROM Discendenti_di_', Pers1, ') AS A, (SELECT * FROM Antenati_di_', Pers2, ' UNION SELECT * from Discendenti_di_', Pers2, ') AS B WHERE A.Antenato = B.Antenato;'); 
	PREPARE trovaGenComune from @A;
	EXECUTE trovaGenComune;
	DEALLOCATE PREPARE trovaGenComune;

	-- Esegue il DROP delle tabelle usate
	call dropTab(concat('Antenati_di_',Pers1));
	call dropTab(concat('Discendenti_di_',Pers1));
	call dropTab(concat('Antenati_di_',Pers2));
	call dropTab(concat('Discendenti_di_',Pers2));
END|
DELIMITER ;

/* Operazione 18 */
DELIMITER |
CREATE PROCEDURE AntenatiEpoca (Ep VARCHAR(12))
/* Stampa tutti gli antenati dei personaggi che appaiono in eventi di una data 
epoca */
BEGIN
	-- Dichiarazioni di CONT e CP
	DECLARE CONT INTEGER; -- Contatore
	DECLARE CP CHAR(4); -- CodicePersonaggio
	
	-- Crea una tabella selezionando tutti i personaggi che sono apparsi 
	-- nell'epoca Ep
	CREATE TEMPORARY TABLE IF NOT EXISTS personaggi AS
		(	SELECT DISTINCT C.CodicePersonaggio AS codice
			FROM Coinvolgimento AS C 
					JOIN 
				Evento AS E ON C.CodiceEvento = E.Codice
			WHERE E.Epoca = Ep);
		
	-- Crea una tabella in cui ci saranno i risultati finali
	CREATE TEMPORARY TABLE Ant LIKE personaggi;

	-- Conta per quanti personaggi devo cercare gli antenati
	SELECT count(*) INTO CONT FROM personaggi;
	
	-- Imposta il limite di ricorsione a 50
	SET max_sp_recursion_depth = 50;
	
	-- Finché ci sono elementi in personaggi
	WHILE CONT > 0
	DO
		-- Prende un personaggio qualunque di personaggi
		SELECT * INTO CP FROM personaggi LIMIT 1;
	
		IF CP NOT IN (SELECT * FROM Ant) THEN	
			-- Trova gli antenati di CP
			call TrovaAntenati(CP);
				
			-- Inserisce in Ant gli antenati trovati
			INSERT INTO Ant SELECT * FROM Antenati;
			
			DROP TABLE Antenati;
		END IF;

		-- Elimina CP da personaggi
		DELETE FROM personaggi WHERE codice = CP;
		
		-- Decrementa il contatore
		SET CONT = CONT - 1;
	END WHILE;
	
	-- Seleziona tutti da Ant
	SELECT DISTINCT P.Codice, P.Nome 
	FROM Ant AS A 
			JOIN 
		Personaggio AS P ON A.codice = P.Codice;
	
	DROP TABLE Ant;
	DROP TABLE personaggi;
	SET max_sp_recursion_depth = 0;
END|
DELIMITER ;
