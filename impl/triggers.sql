/* Regola di Vincolo 1 */
-- Ogni luogo deve essere associato almeno a un evento o a un sito archeologico
DELIMITER |
CREATE TRIGGER AssociazioneEvento_up
BEFORE UPDATE ON Evento
FOR EACH ROW
/* Il trigger gestisce gli aggiornamenti in cascata su Evento e 
SitoArcheologico. Se il numero di collegamenti a Evento o a SitoArcheologico è 
inferiore o uguale a 1 segnala un errore, altrimenti esegue i corretti
aggiornamenti */
BEGIN
	DECLARE cn INTEGER;
	DECLARE co INTEGER;
	DECLARE ErrLuogoR CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Numero associazioni del luogo insufficienti';

	set cn = 0;
	set co = 0;	
	
	IF new.CodiceLuogo <> old.CodiceLuogo
	THEN
		-- conta i riferimenti a eventi avvenuti in old.CodiceLuogo
		SELECT count(*) into cn 
		FROM  Evento
		WHERE old.Codice =  CodiceLuogo;
		
		-- conta i riferimenti a siti archeologici situati in old.CodiceLuogo
		SELECT count(*) into co
		FROM SitoArcheologico
		WHERE old.Codice = CodiceLuogo;
		
		-- se la somma di riferimenti a eventi e siti archeologici è inferiore o 
		-- uguale a 1, impedisce l'aggiornamento dell'evento
		IF cn+co <= 1
		THEN
			SIGNAL ErrLuogoR SET MESSAGE_TEXT = @err_mess;
		END IF;
	END IF;
END|
DELIMITER ;

-- Versione di AssociazioneLuogo_up per la DELETE
DELIMITER |
CREATE TRIGGER AssociazioneEvento_del
BEFORE DELETE ON Evento
FOR EACH ROW
BEGIN
	DECLARE cn INT;
	DECLARE co INT;
	DECLARE ErrLuogoR CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Luogo non associato';

	set cn = 0;
	set co = 0;
	-- conta i riferimenti a eventi avvenuti in old.CodiceLuogo
	SELECT count(*) into cn 
	FROM  Evento
	WHERE old.CodiceLuogo = CodiceLuogo;
	
	-- conta i riferimenti a siti situati in old.CodiceLuogo
	SELECT count(*) into co
	FROM SitoArcheologico
	WHERE old.CodiceLuogo = CodiceLuogo;
	
	-- se la somma dei riferimenti a eventi e siti archeologici è inferiore o 
	-- uguale a 1, impedisce l'eliminazione  dell'evento
	IF(cn+co <=1)
	THEN
		SIGNAL ErrLuogoR SET MESSAGE_TEXT = @err_mess;
	END IF;
END|
DELIMITER ;

DELIMITER |
CREATE TRIGGER AssociazioneSito_up
BEFORE UPDATE ON SitoArcheologico
FOR EACH ROW
/* Il trigger gestisce gli aggiornamenti su SitoArcheologico. Se il numero di 
collegamenti a Evento o a SitoArcheologico è inferiore o uguale a 1 segnala un 
errore, altrimenti esegue i corretti aggiornamenti */
BEGIN
	DECLARE cn INTEGER;
	DECLARE co INTEGER;
	DECLARE ErrLuogoR CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Numero associazioni del luogo insufficienti';

	SET cn = 0;
	SET co = 0;
	
	IF new.CodiceLuogo <> old.CodiceLuogo
	THEN
		-- conta i riferimenti a eventi avvenuti in old.CodiceLuogo
		SELECT count(*) into cn 
		FROM  Evento
		WHERE old.CodiceLuogo = CodiceLuogo;
		
		-- conta i riferimenti a siti archeologici situati in old.CodiceLuogo
		SELECT count(*) into co
		FROM SitoArcheologico
		WHERE old.CodiceLuogo = CodiceLuogo;
		
		-- se la somma di riferimenti a eventi e siti archeologici è inferiore o 
		-- uguale a 1, impedisce l'aggiornamento dell'evento
		IF cn+co <= 1
		THEN
			SIGNAL ErrLuogoR SET MESSAGE_TEXT = @err_mess;
		END IF;
	END IF;
END|
DELIMITER ;

-- Versione di AssociazioneSito_up per la DELETE
DELIMITER |
CREATE TRIGGER AssociazioneSito_del
BEFORE DELETE ON SitoArcheologico
FOR EACH ROW
BEGIN
	DECLARE cn INT;
	DECLARE co INT;
	DECLARE ErrLuogoR CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Luogo non associato';

	-- conta i riferimenti a eventi avvenuti in old.CodiceLuogo
	SELECT count(*) into cn 
	FROM  Evento
	WHERE old.CodiceLuogo = CodiceLuogo;
	
	-- conta i riferimenti a siti situati in old.CodiceLuogo
	SELECT count(*) into co
	FROM SitoArcheologico
	WHERE old.CodiceLuogo = CodiceLuogo;
	
	-- se la somma dei riferimenti a eventi e siti archeologici è inferiore o 
	-- uguale a 1, impedisce l'eliminazione  dell'evento
	IF(cn+co <=1)
	THEN
		SIGNAL ErrLuogoR SET MESSAGE_TEXT = @err_mess;
	END IF;
END|
DELIMITER ;

/* Regola di Vincolo 2 */
-- Ogni fonte deve almeno narrare un evento o delineare un personaggio
DELIMITER |
CREATE TRIGGER NarrazioneFonte_up
BEFORE UPDATE ON Narrazione
FOR EACH ROW
/* Il trigger blocca tutti gli aggiornamenti su Narrazione nel caso in cui il 
numero di collegamenti tra Fonte e Narrazione e tra Fonte e Delineazione è 
inferiore o uguale a 1 */
BEGIN
	DECLARE cn INT;
	DECLARE co INT;
	
	DECLARE ErrNarrF CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Fonte non associata';
	
	IF new.CodiceFonte <> old.CodiceFonte
	THEN	
		-- conto tutte le volte che fonte appare nella tabella narrazione per
		-- vedere se ha altri collegamenti con Evento
		SELECT count(*) into cn 
		FROM  Narrazione
		WHERE old.CodiceFonte =  CodiceFonte;
		
		-- conto tutte le volte che fonte appare nella tabella delineazione per
		-- vedere se ha altri collegamenti con Personaggio
		SELECT count(*) into co 
		FROM Delineazione
		WHERE old.CodiceFonte = CodiceFonte; 					
						
		-- se voglio fare un aggiornamento su Narrazione che coinvolge una fonte 
		-- che ha un solo legame con evento o personaggio, impedisco 
		-- l'aggiornamento
		IF(cn+co <= 1)
		THEN
			SIGNAL ErrNarrF SET MESSAGE_TEXT = @err_mess;
		END IF;
	END IF;
END|
DELIMITER ;

-- Versione per NarrazioneFonte_del
DELIMITER |
CREATE TRIGGER NarrazioneFonte_del
BEFORE DELETE ON Narrazione
FOR EACH ROW
BEGIN
	DECLARE cn INT;
	DECLARE co INT;

	DECLARE ErrNarrF CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Fonte non associata';	

	-- conto tutte le volte che fonte appare nella tabella narrazione per vedere
	-- se ha altri collegamenti con Evento
	SELECT count(*) into cn 
	FROM  Narrazione
	WHERE old.CodiceFonte =  CodiceFonte;
	
	-- conto tutte le volte che fonte appare nella tabella delineazione per 
	-- vedere se ha altri collegamenti con Personaggio
	SELECT count(*) into co
	FROM Delineazione
	WHERE old.CodiceFonte = CodiceFonte;
	
	 -- se voglio fare un'eliminazione su Narrazione che coinvolge una fonte che
	 -- ha un solo legame con evento o personaggio, impedisco l'eliminazione
	IF(cn+co <= 1) 				
	THEN
		SIGNAL ErrNarrF SET MESSAGE_TEXT = @err_mess;
	END IF;
END |
DELIMITER ;

DELIMITER |
CREATE TRIGGER DelineazioneFonte_up
BEFORE UPDATE ON Delineazione
FOR EACH ROW
/* Il trigger blocca tutti gli aggiornamenti su Delineazione nel caso in cui il 
numero di collegamenti tra Fonte e Narrazione e tra Fonte e Delineazione è 
inferiore o uguale a 1 */
BEGIN
	DECLARE cn INT;
	DECLARE co INT;
	DECLARE ErrDelF CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Fonte non associata';
	
	IF new.CodiceFonte <> old.CodiceFonte
	THEN
		-- conto tutte le volte che fonte appare nella tabella narrazione per 
		-- vedere se ha altri collegamenti con Evento
		SELECT count(*) into cn 
		FROM  Narrazione
		WHERE old.CodiceFonte = CodiceFonte;
		
		-- conto tutte le volte che fonte appare nella tabella delineazione per 
		-- vedere se ha altri collegamenti con Personaggio
		SELECT count(*) into co 
		FROM Delineazione
		WHERE old.CodiceFonte = CodiceFonte; 					
						
		 -- se voglio fare un aggiornamento su Delineazione che coinvolge una 
		 -- fonte che ha un solo legame con evento o personaggio, impedisco 
		 -- l'aggiornamento
		IF(cn+co <= 1) 				
		THEN
			SIGNAL ErrDelF SET MESSAGE_TEXT = @err_mess;
		END IF;
	END IF;
END|
DELIMITER ;

-- Versione di DelineazioneFonte_up per la DELETE
DELIMITER |
CREATE TRIGGER DelineazioneFonte_del
BEFORE DELETE ON Delineazione
FOR EACH ROW
BEGIN
	DECLARE cn INT;
	DECLARE co INT;
	DECLARE ErrDelF CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Fonte non associata';
		
	-- conto tutte le volte che fonte appare nella tabella narrazione per vedere
	-- se ha altri collegamenti con Evento
	SELECT count(*) into cn 
	FROM  Narrazione
	WHERE old.CodiceFonte = CodiceFonte;
	
	-- conto tutte le volte che fonte appare nella tabella delineazione per 
	-- edere se ha altri collegamenti con Personaggio
	SELECT count(*) into co 
	FROM Delineazione
	WHERE old.CodiceFonte = CodiceFonte; 					
					
	 -- se voglio fare un'eliminazione su  Delineazione che coinvolge una fonte 
	 -- che ha un solo legame con evento o personaggio, impedisco il delete
	IF(cn+co <= 1) 				
	THEN
		SIGNAL ErrDelF SET MESSAGE_TEXT = @err_mess;
	END IF;
END|
DELIMITER ;

/* Regola di Vincolo 3 */
-- Tutte le semidivinità e tutte le divinità devono avere almeno un genitore 
-- (tranne Chaos)
DELIMITER |
CREATE TRIGGER GenitoreDivinita
BEFORE DELETE ON Genitorialita
FOR EACH ROW
/* Il trigger impedisce di eliminare tutti i genitori di una divinità */
BEGIN
	DECLARE T VARCHAR (17);
	DECLARE ErrGenitD CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Errore nella genealogia: non cancellare unico genitore';
	
	SELECT Tipo INTO T
	FROM FiguraMitologica
	WHERE Codice = old.Figlio;
	
	IF T = 'Divinita' OR T = 'Semidivinita/Eroe'
	THEN
		IF NOT EXISTS 
					-- se non è vero che la divinità ha almeno 2 genitori (cioé 
					-- ne ha solo uno) e che il suo nome, allora ne impediamo 
					-- l'eliminazione
			( -- la query è non vuota sse old.Figlio è figlio di almeno un altro 
			 -- genitore diverso da old.Genitore ( se old.Figlio ha 2 genitori)
				SELECT *
				FROM Genitorialita as G1
				WHERE old.Genitore <> G1.Genitore AND G1.Figlio=old.Figlio 
			)
		THEN
			SIGNAL ErrGenitD SET MESSAGE_TEXT = @err_mess;
		END IF;
	END IF;
END |
DELIMITER ;

DELIMITER |
CREATE TRIGGER CoerGen_ins
BEFORE INSERT ON Genitorialita
FOR EACH ROW
BEGIN
	DECLARE AN_Gen SMALLINT; -- Anno di nascita del genitore
	DECLARE AN_Fig SMALLINT; -- Anno di nascita del figlio
	DECLARE N_Gen INTEGER; -- Numero Genitori
	DECLARE ErrGen CONDITION FOR SQLSTATE '45000';
	DECLARE EtaErr CONDITION FOR SQLSTATE '45000';
	
	
	-- Regola di Vincolo 3
	-- Il trigger impedisce che venga inserito Chaos come figlio di qualcuno
	IF new.Figlio IN (SELECT Codice FROM Personaggio WHERE Nome = 'Chaos')
	THEN
		set @err_mess = 'Errore nella genealogia: Chaos non può avere genitori';
		SIGNAL ErrGen SET MESSAGE_TEXT = @err_mess;
	END IF;

	-- Regola di Vincolo 4
	-- Il trigger impedisce che l'anno di nascita dell'essere umano inserito sia
	-- minore di quelli dei genitori	
	IF new.Figlio IN (SELECT Codice FROM EssereUmano WHERE Tipo = 'Esistito')
	THEN
		-- Imposta AN_Gen e AN_Fig con l'anno di nascita del primo genitore e 
		-- del figlio
		SELECT AnnoNascita INTO AN_Gen
		FROM EssereUmano
		WHERE Codice = new.Genitore;

		SELECT AnnoNascita INTO AN_Fig
		FROM EssereUmano
		WHERE Codice = new.Figlio;

		-- Se AN_Gen è maggiore di AN_Fig
		IF (AN_Gen > AN_Fig)
		THEN
			set @err_mess = concat('Errore di età: ',new.Genitore,' è più giovane di ',new.Figlio);
			SIGNAL EtaErr SET MESSAGE_TEXT = @err_mess;
		END IF;
	END IF;
	
	
	-- Regola di Vincolo 7
	-- Il trigger impedisce che un personaggio abbia più di due genitori
	
	-- Conta il numero di genitori di new.Figlio
	SELECT count(*) INTO N_Gen
	FROM Genitorialita
	WHERE Figlio = new.Figlio;
	
	-- Se ha 2 genitori, non è possibile aggiungerne altri e genera un errore
	IF N_Gen >= 2
	THEN
		set @err_mess = 'Errore nella genealogia: limite genitori superato';
		SIGNAL ErrGen SET MESSAGE_TEXT = @err_mess;
	END IF;


	-- Regola di Vincolo 5
	-- Il trigger non permette che la coppia (Genitore, Figlio) inserita si trovi
	-- tra i discendenti e tra gli antenati

	-- Trova gli antenati del genitore
	set max_sp_recursion_depth = 50;
	call TrovaAntenati(new.Genitore);
	set max_sp_recursion_depth = 0;

	-- Se figlio e padre coincidono oppure il figlio spunta tra gli Antenati del
	-- padre viene segnalato un errore
	IF new.Figlio = new.Genitore OR new.Figlio IN (	SELECT * FROM Antenati)
	THEN
		DELETE FROM Antenati;
		
		set @err_mess = concat('Errore nella genealogia: ',new.Figlio,' è antenato di se stesso');
		SIGNAL ErrGen SET MESSAGE_TEXT = @err_mess;
	END IF;
	DELETE FROM Antenati;
END|
DELIMITER ;

/* Regola di Vincolo 4 */

DELIMITER |
CREATE TRIGGER CoerEssUm_up
BEFORE UPDATE ON EssereUmano
FOR EACH ROW
BEGIN
	DECLARE AN_Gen SMALLINT;
	DECLARE AN_Fig SMALLINT;
	DECLARE EtaErr CONDITION FOR SQLSTATE '45000';
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';

	-- Impedisce l'aggiornamento di un codice in modo errato	
	IF new.Codice NOT LIKE 'UE%' AND new.Codice NOT LIKE 'UF%'
	THEN
		set @err_mess = 'Codice incoerente in EssereUmano';
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	END IF;
	
	-- Impedisce che un personaggio abbia data di nascita inferiore a quella dei
	-- genitori
	IF new.Tipo = 'Esistito'
	THEN
		SELECT max(E.AnnoNascita) INTO AN_Gen
		FROM EssereUmano AS E
				JOIN
			Genitorialita AS G ON E.Codice = G.Genitore
		WHERE G.Figlio = new.Codice;
		
		SELECT min(E.AnnoNascita) INTO AN_Fig
		FROM EssereUmano AS E
				JOIN
			Genitorialita AS G ON E.Codice = G.Figlio;
		
		IF (AN_Gen IS NOT NULL AND AN_Gen > new.AnnoNascita)
		THEN
			set @err_mess = concat('Errore di età: ',new.Codice,' è più vecchio di uno dei suoi genitori');
			SIGNAL EtaErr SET MESSAGE_TEXT = @err_mess;
		END IF;
		
		IF (AN_Fig IS NOT NULL AND AN_Fig < new.AnnoNascita)
		THEN
			set @err_mess = concat('Errore di età: ',new.Codice,' è più giovane di uno dei suoi figli');
			SIGNAL EtaErr SET MESSAGE_TEXT = @err_mess;
		END IF;
	END IF;
END|
DELIMITER ;

/* Regola di Vincolo 5 e Regola di Vincolo 7 */
-- Ogni personaggio può avere al più due genitori e può avere al più 2 genitori

DELIMITER |
CREATE TRIGGER CoerGen_up
BEFORE UPDATE ON Genitorialita
FOR EACH ROW
BEGIN
	DECLARE N_Gen INTEGER; -- Numero Genitori
	DECLARE GenErr CONDITION FOR SQLSTATE '45000';
	
	
	-- Regola di Vincolo 7
	-- Impedisce che un personaggio abbia più di due genitori
	
	-- Conta il numero di genitori di new.Figlio
	SELECT count(*) INTO N_Gen
	FROM Genitorialita
	WHERE Figlio = new.Figlio;
	
	-- Se ha 2 genitori, non è possibile aggiungerne altri e genera un errore
	IF N_Gen >= 2
	THEN
		set @err_mess = 'Errore nella genealogia: limite genitori superato';
		SIGNAL GenErr SET MESSAGE_TEXT = @err_mess;
	END IF;
	
	
	-- Regola di Vincolo 5
	-- Impedisce che il personaggio sia antenato di se stesso
	set max_sp_recursion_depth = 50;
	call TrovaAntenati(new.Genitore);
	set max_sp_recursion_depth = 0;

	IF new.Figlio = new.Genitore OR new.Figlio IN (	SELECT * FROM Antenati)
	THEN
		set @err_mess = concat('Errore nella genealogia: ',new.Figlio,' è antenato di se stesso');
		SIGNAL GenErr SET MESSAGE_TEXT = @err_mess;
	END IF;
	DELETE FROM Antenati;
END|
DELIMITER ;

/* Trigger sulla coerenza dei codici */

-- EssereUmano
DELIMITER | 
CREATE TRIGGER CoerEssUm_ins
BEFORE INSERT ON EssereUmano
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Codice incoerente in EssereUmano';
	
	IF new.Codice NOT LIKE 'UE%' AND new.Codice NOT LIKE 'UF%'
	THEN
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	ELSE
		IF new.Codice LIKE 'UE%' AND new.Tipo <> 'Esistito'
		THEN
			SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
		END IF;

		IF new.Codice LIKE 'UF%' AND new.Tipo <> 'Fittizio'
		THEN
			SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
		END IF;
	END IF;
END|
DELIMITER ;

-- FiguraMitologica
DELIMITER | 
CREATE TRIGGER CoerFigMit_ins
BEFORE INSERT ON FiguraMitologica
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = concat('Codice incoerente in FiguraMitologica');

	IF (new.Codice NOT LIKE 'MD%' AND new.Codice NOT LIKE 'MS%' 
		AND new.Codice NOT LIKE 'MC%')
	THEN
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	ELSE
		IF new.Codice LIKE 'MD%' AND new.Tipo <> 'Divinita'
		THEN
			SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
		END IF;

		IF new.Codice LIKE 'MS%' AND new.Tipo <> 'Semidivinita/Eroe'
		THEN
			SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
		END IF;
		
		IF new.Codice LIKE 'MC%' AND new.Tipo <> 'Creatura'
		THEN
			SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
		END IF;
	END IF;
		
END|
DELIMITER ;

DELIMITER | 
CREATE TRIGGER CoerFigMit_up
BEFORE UPDATE ON FiguraMitologica
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Codice incoerente in FiguraMitologica';

	IF new.Codice NOT LIKE 'MD%' AND new.Codice NOT LIKE 'MS%' 
		AND new.Codice NOT LIKE 'MC%'
	THEN 
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	ELSE
		IF new.Codice LIKE 'MD%' AND new.Tipo <> 'Divinit_'
		THEN
			SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
		END IF;

		IF new.Codice LIKE 'MS%' AND new.Tipo <> 'Semidivinit_/Eroe'
		THEN
			SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
		END IF;
		
		IF new.Codice LIKE 'MC%' AND new.Tipo <> 'Creatura'
		THEN
			SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
		END IF;
	END IF;

END|
DELIMITER ;

-- Divinità
DELIMITER | 
CREATE TRIGGER CoerDiv_ins
BEFORE INSERT ON Divinita
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Codice incoerente in Divinita';

	IF new.Codice NOT LIKE 'MD%'
	THEN
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	END IF;
END|
DELIMITER ;

DELIMITER | 
CREATE TRIGGER CoerDiv_up
BEFORE UPDATE ON Divinita
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Codice incoerente in Divinita';

	IF new.Codice NOT LIKE 'MD%'
	THEN
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	END IF;
END|
DELIMITER ;

-- Creatura
DELIMITER | 
CREATE TRIGGER CoerCre_ins
BEFORE INSERT ON Creatura
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Codice incoerente in Creatura';

	IF new.Codice NOT LIKE 'MC%'
	THEN
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	END IF;
END|
DELIMITER ;

DELIMITER | 
CREATE TRIGGER CoerCre_up
BEFORE UPDATE ON Creatura
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Codice incoerente in Creatura';

	IF new.Codice NOT LIKE 'MC%'
	THEN
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	END IF;
END|
DELIMITER ;

-- Luogo
DELIMITER | 
CREATE TRIGGER CoerLuo_ins
BEFORE INSERT ON Luogo
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Codice incoerente in Luogo';

	IF new.Codice NOT LIKE 'R%' AND new.Codice NOT LIKE 'M%'
	THEN
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	ELSE
		IF new.Codice LIKE 'R%' AND new.Tipo <> 'Reale'
		THEN
			SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
		END IF;
		
		IF new.Codice LIKE 'M%' AND new.Tipo <> 'Mitologico'
		THEN
			SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
		END IF;
	END IF;
END|
DELIMITER ;

DELIMITER | 
CREATE TRIGGER CoerLuo_up
BEFORE UPDATE ON Luogo
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Codice incoerente in Luogo';

	IF new.Codice NOT LIKE 'R%' AND new.Codice NOT LIKE 'M%'
	THEN
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	ELSE
		IF new.Codice LIKE 'R%' AND new.Tipo <> 'Reale'
		THEN
			SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
		END IF;
		
		IF new.Codice LIKE 'M%' AND new.Tipo <> 'Mitologico'
		THEN
			SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
		END IF;
	END IF;
END|
DELIMITER ;

-- LuogoReale
DELIMITER | 
CREATE TRIGGER CoerLuoRe_ins
BEFORE INSERT ON LuogoReale
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Codice incoerente in LuogoReale';

	IF new.CodiceLuogo NOT LIKE 'R%'
	THEN
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	END IF;
END|
DELIMITER ;

DELIMITER | 
CREATE TRIGGER CoerLuoRe_up
BEFORE UPDATE ON LuogoReale
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Codice incoerente in LuogoReale';

	IF new.CodiceLuogo NOT LIKE 'R%'
	THEN
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	END IF;
END|
DELIMITER ;

-- Autore
DELIMITER | 
CREATE TRIGGER CoerAut_ins
BEFORE INSERT ON Autore
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Codice incoerente in Autore';

	IF new.Codice NOT LIKE 'W%' AND new.Codice NOT LIKE 'S%' 
		AND new.Codice NOT LIKE 'P%'
	THEN
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	ELSE
		IF new.Codice LIKE 'W%' AND new.Tipo <> 'Scrittore'
		THEN
			SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
		END IF;
		
		IF new.Codice LIKE 'S%' AND new.Tipo <> 'Scultore'
		THEN
			SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
		END IF;
		
		IF new.Codice LIKE 'P%' AND new.Tipo <> 'Pittore'
		THEN
			SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
		END IF;
	END IF;
END|
DELIMITER ;

DELIMITER | 
CREATE TRIGGER CoerAut_up
BEFORE UPDATE ON Autore
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Codice incoerente in Autore';

	IF new.Codice NOT LIKE 'W%' AND new.Codice NOT LIKE 'S%' 
		AND new.Codice NOT LIKE 'P%'
	THEN
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	END IF;
END|
DELIMITER ;

-- Fonte
DELIMITER | 
CREATE TRIGGER CoerFon_ins
BEFORE INSERT ON Fonte
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Codice incoerente in Fonte';

	IF new.Codice NOT LIKE 'S%' AND new.Codice NOT LIKE 'A%' 
	THEN
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	END IF;
END|
DELIMITER ;

DELIMITER | 
CREATE TRIGGER CoerFon_up
BEFORE UPDATE ON Fonte
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Codice incoerente in Fonte';

	IF new.Codice NOT LIKE 'S%' AND new.Codice NOT LIKE 'A%' 
	THEN
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	END IF;
END|
DELIMITER ;

-- FonteArcheologica
DELIMITER | 
CREATE TRIGGER CoerFonArc_ins
BEFORE INSERT ON FonteArcheologica
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Codice incoerente in FonteArcheologica';

	IF new.Codice NOT LIKE 'A%' 
	THEN
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	END IF;
END|
DELIMITER ;

DELIMITER | 
CREATE TRIGGER CoerFonArc_up
BEFORE UPDATE ON FonteArcheologica
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Codice incoerente in FonteArcheologica';

	IF new.Codice NOT LIKE 'A%' 
	THEN
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	END IF;
END|
DELIMITER ;

-- FonteScritta
DELIMITER | 
CREATE TRIGGER CoerFonScr_ins
BEFORE INSERT ON FonteScritta
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Codice incoerente in FonteScritta';

	IF new.Codice NOT LIKE 'S%' 
	THEN
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	END IF;
END|
DELIMITER ;

DELIMITER | 
CREATE TRIGGER CoerFonScr_up
BEFORE UPDATE ON FonteScritta
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Codice incoerente in FonteScritta';

	IF new.Codice NOT LIKE 'S%' 
	THEN
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	END IF;
END|
DELIMITER ;

-- RapprModerna
DELIMITER | 
CREATE TRIGGER CoerRapMod_ins
BEFORE INSERT ON RapprModerna
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Codice incoerente in RapprModerna';

	IF new.Codice NOT LIKE 'A%' AND new.Codice NOT LIKE 'F%' 
		AND new.Codice NOT LIKE 'L%'
	THEN
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	ELSE
		IF new.Codice LIKE 'A%' AND new.Tipo <> 'Artistica'
		THEN
			SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
		END IF;
		
		IF new.Codice LIKE 'F%' AND new.Tipo <> 'Film'
		THEN
			SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
		END IF;
		
		IF new.Codice LIKE 'L%' AND new.Tipo <> 'Letteraria'
		THEN
			SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
		END IF;
	END IF;
END|
DELIMITER ;

DELIMITER | 
CREATE TRIGGER CoerRapMod_up
BEFORE UPDATE ON RapprModerna
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Codice incoerente in RapprModerna';

	IF new.Codice NOT LIKE 'A%' AND new.Codice NOT LIKE 'F%' 
		AND new.Codice NOT LIKE 'L%'
	THEN
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	ELSE
		IF new.Codice LIKE 'A%' AND new.Tipo <> 'Artistica'
		THEN
			SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
		END IF;
		
		IF new.Codice LIKE 'F%' AND new.Tipo <> 'Film'
		THEN
			SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
		END IF;
		
		IF new.Codice LIKE 'L%' AND new.Tipo <> 'Letteraria'
		THEN
			SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
		END IF;
	END IF;
END|
DELIMITER ;

-- RapprArtistica
DELIMITER | 
CREATE TRIGGER CoerRapArt_ins
BEFORE INSERT ON RapprArtistica
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Codice incoerente in RapprArtistica';

	IF new.Codice NOT LIKE 'A%'
	THEN
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	END IF;
		
END|
DELIMITER ;

DELIMITER | 
CREATE TRIGGER CoerRapArt_up
BEFORE UPDATE ON RapprArtistica
FOR EACH ROW
BEGIN
	DECLARE CodErr CONDITION FOR SQLSTATE '45000';
	set @err_mess = 'Codice incoerente in RapprArtistica';

	IF new.Codice NOT LIKE 'A%'
	THEN
		SIGNAL CodErr SET MESSAGE_TEXT = @err_mess;
	END IF;
END|
DELIMITER ;		

DELIMITER ;
