/* Operazione 12 */
DELIMITER |
CREATE FUNCTION NumeroLuoghi (E VARCHAR(12)) RETURNS INTEGER
/* data un’epoca E, restituisce il numero di luoghi reali in cui è avvenuto 
almeno un evento dell’epoca E, ma che non ospitano siti archeologici. Nel caso 
in cui tale numero sia 0, restituisce quello di luoghi mitologici */
BEGIN
	DECLARE numero INT;
	SELECT count(distinct LR.CodiceLuogo ) INTO numero
	FROM Evento as EV, LuogoReale as LR
	WHERE EV.Epoca=E AND EV.CodiceLuogo=LR.CodiceLuogo 
		AND LR.CodiceLuogo NOT IN (	SELECT CodiceLuogo 
									FROM SitoArcheologico);
									
	IF numero = 0
	THEN
		SELECT count(distinct LR.CodiceLuogo ) INTO numero
		FROM Evento as EV, Luogo as L
		WHERE EV.Epoca=E AND EV.CodiceLuogo = L.Codice AND L.Tipo = 'Mitologico';
	END IF;

	RETURN numero;
END|
DELIMITER ;

/* Operazione 13 */
DELIMITER |
CREATE FUNCTION NumeroFontiStessaCitta(NomeEv VARCHAR(40), Ep VARCHAR(12))
																RETURNS INTEGER
/* dato un nome di evento N e un'epoca E, se l'epoca è quella degli dei, 
restituisce il numero di fonti di tale evento, il cui autore è nato e morto 
nella stessa città. Altrimenti quello delle fonti il cui autore è nato e morto 
in città diverse */
BEGIN
	DECLARE NumF INTEGER;
	
	IF Ep = 'Dei'
	THEN
		SELECT count(*) INTO NumF
		FROM ((	Evento AS E 
					JOIN 
				Narrazione AS N ON E.Codice = N.CodiceEvento)
					JOIN
				Fonte AS F ON N.CodiceFonte = F.Codice)
					JOIN
				Autore AS A ON F.CodiceAutore = A.Codice
		WHERE E.Nome = NomeEv AND E.Epoca = Ep AND A.CittaOrigine = A.CittaDecesso;
	ELSE
		SELECT count(*) INTO NumF
		FROM ((	Evento AS E 
					JOIN 
				Narrazione AS N ON E.Codice = N.CodiceEvento)
					JOIN
				Fonte AS F ON N.CodiceFonte = F.Codice)
					JOIN
				Autore AS A ON F.CodiceAutore = A.Codice
		WHERE E.Nome = NomeEv AND E.Epoca = Ep AND A.CittaOrigine <> A.CittaDecesso;
	END IF;
	
	RETURN NumF;
END|
DELIMITER ;

/* Operazione 14 */
DELIMITER |
CREATE FUNCTION Person_NumNipoti_Magg(S ENUM('F','M')) RETURNS VARCHAR(20)
/* dato il sesso S di un personaggio, restituisce il codice del personaggio col 
maggior numero di nipoti (i figli dei fratelli o delle sorelle) maschi 
che sono divinità. In caso ci siano più personaggi con lo stesso numero di 
nipoti ne restituisce uno qualunque */
BEGIN
	DECLARE COD CHAR(4);

	SELECT DISTINCT G1.Figlio INTO COD
	FROM (((Genitorialita AS G1
			JOIN
		Genitorialita AS G2 ON G1.Genitore = G2.Genitore) -- Fratelli G1 e G2 
			JOIN
		Genitorialita AS G3 ON G2.Figlio = G3.Genitore) -- Nipoti G3,figli di G2
			JOIN
		Personaggio AS Z ON G1.Figlio = Z.Codice) -- Informazioni dello zio
			JOIN
		Personaggio AS N ON G3.Figlio = N.Codice -- Informazioni del nipote
	WHERE G1.Figlio <> G2.Figlio AND Z.Sesso = S AND N.Sesso = 'M'
			AND N.Codice IN ( SELECT Codice FROM Divinita )
	GROUP BY G1.Figlio
	HAVING count(distinct G3.Figlio) >= ALL(
							SELECT count(distinct G3.Figlio)
							FROM (((Genitorialita AS G1
										JOIN
								Genitorialita AS G2 ON G1.Genitore=G2.Genitore)
										JOIN
								Genitorialita AS G3 ON G2.Figlio = G3.Genitore)
										JOIN
								Personaggio AS Z ON G1.Figlio = Z.Codice)
										JOIN
								Personaggio AS N ON G3.Figlio = N.Codice
							WHERE G1.Figlio <> G2.Figlio AND Z.Sesso = S AND 
								N.Sesso = 'M' AND N.Codice IN (	SELECT Codice 
															   	FROM Divinita)
							GROUP BY G1.Figlio )
	LIMIT 1;
	
	RETURN COD;
END|
DELIMITER ;
